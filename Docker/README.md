## Docker Engine & Docker Compose
![alt](img/logo.png "Docker logo")

* Readme:  https://docs.docker.com/engine/install/ubuntu/

### Base commands:

```bash
docker ps -a		              #show all docker containers
docker images	                #show local images 
docker search <> 	            #search image in registry (DockerHub)
docker pull <> 	              #download image from registry to localhost
docker build -t myimage . 	  #build image from Dockerfile
docker run <> 	              #run container
docker rm <> 	                #remove container
docker rmi <>	                #remove image
docker logs <> 	              #logs of container
docker start/stop/restart <>  #work with container
docker attach <>              #attach to runing container
docker inspect <>             #info about image or container
docker tag myimage:v1 myimage:copy	  #dublicate image with new tag
docker commit 6ew7ewe889s myimage:v2	#copy running container
docker exec -it 6re789eer /bin/bash	  #enter to container
```


### Advanced commands:

Run ubuntu container interactively & attached
```bash
docker run -it ubuntu /bin/bash
```

Run ubuntu container & and remove after stop
```bash
docker run --rm ubuntu 
```

Run in detached mode & ports forwarding from 8080 to 1234 
```bash
docker run -d -p 1234:8080 myimage 
```

Run detached with new name and REMOVE after exited
```bash
docker run --name container1 -d --rm myimage
```

Run with TimeZone environment
```bash
docker run --name container1 -e TZ=Europe/Kyiv myimage
```

Run with mount external volume
```bash
docker run --name container1 -v /var/mylocaldir:/var/projectdir myimage
```

Remove all containers by ID
```bash
docker rm -f $(docker ps -qa)
```

Build own Docker image
```bash
docker build -t myimage:v1 .
```

Remove all images by ID
```bash
docker rmi $(docker images -q)
```

Info about image
```bash
docker image history myimage
```


### Work with volumes
```bash
docker volume ls
docker volume create newvolume
docker run -v newvolume:/var/myproject myimage
```

#### Persistent volumes
```bash
docker run -it -v /home/user/shared-data:/shared-data ubuntu bash
```

#### Ephemeral (shared)
```bash
docker run -it -v /shared-data ubuntu bash         #first container
docker run -it --volumes-from 92b1g23 ubuntu bash  #second container
```

### Work with network
```bash
docker network ls
docker network create mynetwork
docker run --network mynetwork myimage
```


### Dockerfile samples

* usable parameters
```bash
FROM image_from_hub:tag

EXPOSE 80

ENV TZ=Europe/Kyiv
USER 1001

WORKDIR /var/httpd
COPY <source> <destination>
VOLUME ["/dir1","/dir2"] 

RUN apt update && apt install -y apache2
CMD ["",""]
ENTRYPOINT ["echo", "$VAR1"]
```

* sample
```bash
FROM python:3.10
WORKDIR /usr/src/app
EXPOSE 8000
COPY requirements.txt .
RUN pip3 install -qr requirements.txt
COPY app.py .
CMD ["python3", "app.py"]
```



## Docker Compose
![alt](img/compose.png "Docker Compose logo")

* Readme: https://docs.docker.com/compose/

* Sample: docker-compose.yaml

```bash
version: "3.7"
services:
  web:
    build: web
    restart: always
    ports:
      - 5000:5000
    environment:
      - DEBUG=1
    networks:
      - flask_network
    depends_on:
      - db
  proxy:
    build: proxy
    restart: always
    ports:
      - 80:80
    networks:
      - flask_network
  db:
    image: postgres:13
    restart: always
    ports:
      - 5432:5432
    networks:
      - flask_network
    environment:
      - POSTGRES_USER=admin
      - POSTGRES_PASSWORD=admin
      - POSTGRES_DB=market
    volumes:
      - postgres_data:/var/lib/postgresql/data

networks:
  flask_network:

volumes:
  postgres_data:
```

### Commands
```bash
docker-compose up -d //run in detached mode
docker-compose down  //stop and remove containers
docker-compose start/stop/restart
docker-compose ps
docker-compose top
docker-compose logs
```

## DockerHub 

* Sing up & create repos (https://hub.docker.com/)

* Create token and login:		docker login -u vvborys

* Build image with name and tag: 	docker build -t vvborys/ub20apache:v1 .

* Re-tagging an existing local image docker tag <existing-image> <hub-user>/<repo-name>[:<tag>]

* Push image:				docker push vvborys/ub20apache:v1

* Pull exising image:			docker pull vvborys/ub20apache:v1


## Info

* https://www.youtube.com/watch?v=3c-iBn73dDE

