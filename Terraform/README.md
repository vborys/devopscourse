# Terraform
![alt](img/terraform-logo.jpg "logo")

#### Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.


## Basic start by AWS provider
 
https://learn.hashicorp.com/tutorials/terraform/aws-build

https://www.terraform.io/cli
  

* Install Terraform
https://learn.hashicorp.com/tutorials/terraform/install-cli

* Install AWS CLI
https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html

* Add security credentials (Access Key)
1) https://us-east-1.console.aws.amazon.com/iam/home#/security_credentials

2) Run:
```
$ aws configure
```

* Create main.tf

```
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region  = "us-east-1"
}

resource "aws_instance" "app_server" {
  ami           = "ami-08d4ac5b634553e16"
  instance_type = "t2.micro"

  tags = {
    Name = "ExampleAppServerInstance"
  }
}
```
* Run: 
```
$ terraform init

$ terraform plan

$ terraform apply
```


## Basic Terraform CLI commands: 

* terraform init

* terraform plan

* terraform apply (yes) // --auto-approve

* terraform show 

* terraform destroy


## Terraform providers registry

* https://registry.terraform.io/browse/providers

## Terraform providers documentation (AWS)

* https://registry.terraform.io/providers/hashicorp/aws/latest/docs


## SAMPLE 1. Create droplet by DigitalOcean Provider

### Terraform file: "digitallocen_droplet.tf"
```bash
terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "2.11.0"
    }
  }
}

variable "do_token" {}
variable "pvt_key" {}

provider "digitalocean" {
    token = var.do_token

}

resource "digitalocean_droplet" "server" {
  image  = "ubuntu-20-04-x64"
  name   = "server"
  region = "fra1"
  size   = "s-1vcpu-1gb"
  ssh_keys = [" <fingerprint> "]

connection {
    host = self.ipv4_address
    user = "root"
    type = "ssh"
    private_key = file(var.pvt_key)
    timeout = "2m"
  }


  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      # install nginx
      "sudo apt update",
      "sudo apt install -y nginx"
    ]
  }
}

```

### Terraform advanced commands
```
$ terraform init

$ terraform plan -var "do_token=${DO_PAT}" -var "pvt_key=$HOME/.ssh/id_rsa"

$ terraform apply -var "do_token=${DO_PAT}" -var "pvt_key=$HOME/.ssh/id_rsa"

```

## SAMPLE 2. Create droplet by AWS Provider

### Terraform file: "aws_httpd.tf"
```bash
provider "aws" {
  region = "eu-central-1"
}


resource "aws_instance" "my_webserver" {
  ami                    = "ami-03a71cec707bfc3d7"
  instance_type          = "t3.micro"
  vpc_security_group_ids = [aws_security_group.my_webserver.id]

# This is bash script-----------------  
  user_data              = <<EOF
#!/bin/bash
yum -y update
yum -y install httpd
myip=`curl http://169.254.169.254/latest/meta-data/local-ipv4`
echo "<h2>WebServer with IP: $myip</h2><br>Build by Terraform!"  >  /var/www/html/index.html
sudo service httpd start
chkconfig httpd on
EOF
#-------------------------------------
}


resource "aws_security_group" "my_webserver" {
  name = "WebServer Security Group"
  description = "My First SecurityGroup"

#Input & output server rules
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

```

## SAMPLE 3. Create VPC by DO Provider
```bash
resource "digitalocean_vpc" "example_vpc" {
  name     = "example-project-network"
  region   = "fra1"
#  ip_range = "10.114.0.0/20"
}

resource "digitalocean_droplet" "server" {
  name     = "www-01"
  size     = "s-1vcpu-1gb"
  image    = "ubuntu-20-04-x64"
  region   = "fra1"
  vpc_uuid = digitalocean_vpc.example_vpc.id
}

```

### Info

* https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean
* https://docs.digitalocean.com/reference/terraform/getting-started/
* https://youtu.be/UqxebzWKigY
* https://awsregion.info/



### VPC info
* https://registry.terraform.io/providers/digitalocean/digitalocean/latest/docs/resources/vpc
* https://docs.digitalocean.com/products/networking/vpc/
* https://docs.digitalocean.com/products/networking/vpc/resources/best-practices/
* https://www.youtube.com/watch?v=nbo5HrmZjXo
* https://youtu.be/Q3Dxtkgsh9I  -->> (https://github.com/do-community/terraform-sample-digitalocean-architectures/tree/master/01-minimal-web-db-stack)






### Terraform with libvirt
https://youtu.be/uAMzDa_0-pE
https://github.com/dmacvicar/terraform-provider-libvirt
https://registry.terraform.io/providers/dmacvicar/libvirt/latest
https://dev.to/ruanbekker/terraform-with-kvm-2d9e






