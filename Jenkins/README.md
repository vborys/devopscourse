# Jenkins
![logo](img/logo.png "jenkins logo")

### About
> The leading open source automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project.

### Install
* https://www.jenkins.io/doc/book/installing/linux/

* Ubuntu 20.04 example:
```
#Add repos
sudo apt install net-tools curl
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee \
curl -fsSL https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo tee /usr/share/keyrings/jenkins-keyring.asc > /dev/null
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] https://pkg.jenkins.io/debian-stable binary/ | sudo tee /etc/apt/sources.list.d/jenkins.list > /dev/null

sudo apt-get update

#Install JDK & Jenkins
sudo apt install openjdk-11-jre
sudo apt-get install jenkins

#Enable service
sudo systemctl enable jenkins
sudo systemctl start jenkins

#Start in browser and add initPass
sudo cat /var/lib/jenkins/secrets/initialAdminPassword

#Install Maven
sudo apt install maven
echo $JAVA_HOME
readlink -f $(which java)
cat /etc/environment
echo "JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64/" | sudo tee -a /etc/environment
source /etc/environment
echo $JAVA_HOME
mvn --version

sudo systemctl restart jenkins
```




## Pipeline syntax overview

### Jenkinsfile (Declarative Pipeline) example
```
pipeline { 
    agent any 
    options {
        skipStagesAfterUnstable()
    }
    stages {
        stage('Build') { 
            steps { 
                sh 'make' 
            }
        }
        stage('Test'){
            steps {
                sh 'make check'
                junit 'reports/**/*.xml' 
            }
        }
        stage('Deploy') {
            steps {
                sh 'make publish'
            }
        }
    }
}
```

### Jenkinsfile (Scripted Pipeline by Groovy syntax)
```
node {  
    stage('Build') { 
        // ...
    }
    stage('Test') { 
        // ...
    }
    stage('Deploy') { 
        // ...
    }
}
```


